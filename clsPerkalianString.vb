﻿Option Explicit On
Option Strict On

Public Class clsPerkalian
    Private varSisa As Integer
    Private arrayHasil(0, 0) As Integer
    Private Const siKoma As String = "."

    Public Function perkalianString(ByVal string1 As String, ByVal string2 As String) As String
        Dim indexAtas As Integer
        Dim indexBawah As Integer
        Dim angkaBawah As Integer
        Dim angkaAtas As Integer
        Dim hasilPerkalian As Integer
        Dim hasilPenambahan As Integer
        Dim satuan As Integer
        Dim len1 As Integer
        Dim len2 As Integer
        Dim kolomCell As Integer
        Dim barisCell As Integer
        Dim tempString As String
        Dim jumlahAngkaDibelakangKoma1 As Integer
        Dim jumlahAngkaDibelakangKoma2 As Integer
        Dim tandaAkhir As String

        'string1 = CStr(Math.Round(Val(string1), 50))
        'string2 = CStr(Math.Round(Val(string2), 50))

        jumlahAngkaDibelakangKoma1 = zAngkaDibelakangKoma(string1, siKoma)
        jumlahAngkaDibelakangKoma2 = zAngkaDibelakangKoma(string2, siKoma)

        tandaAkhir = zTanda(string1, string2)

        string1 = Replace(Replace(string1, siKoma, ""), "-", "")
        string2 = Replace(Replace(string2, siKoma, ""), "-", "")

        len1 = Len(string1)
        len2 = Len(string2)

        ReDim arrayHasil(len2, len1 + len2 + 1)

        For indexBawah = len2 To 1 Step -1
            angkaBawah = CInt(Mid(string2, indexBawah, 1))
            kolomCell = len1 + indexBawah
            For indexAtas = len1 To 1 Step -1
                angkaAtas = CInt(Mid(string1, indexAtas, 1))
                hasilPerkalian = zGetSisa() + angkaAtas * angkaBawah
                satuan = hasilPerkalian Mod 10
                zPutSisa((hasilPerkalian - satuan) \ 10)
                arrayHasil(indexBawah, kolomCell) = satuan
                kolomCell -= 1
            Next
            If varSisa <> 0 Then
                arrayHasil(indexBawah, kolomCell) = zGetSisa()
            End If
        Next

        For kolomCell = len1 + len2 To 1 Step -1
            hasilPenambahan = zGetSisa()
            For barisCell = len2 To 1 Step -1
                hasilPenambahan += arrayHasil(barisCell, kolomCell)
            Next
            satuan = hasilPenambahan Mod 10
            zPutSisa(CInt((hasilPenambahan - satuan) / 10))
            tempString = satuan & tempString
        Next

        tempString = Left(tempString, Len(tempString) - (jumlahAngkaDibelakangKoma1 + jumlahAngkaDibelakangKoma2)) & "." & Right(tempString, jumlahAngkaDibelakangKoma1 + jumlahAngkaDibelakangKoma2)


        Dim inta As Integer
        For inta = 1 To Len(tempString)
            If Mid(tempString, inta, 1) <> "0" Then
                tempString = Mid(tempString, inta)
                Exit For
            End If
        Next

        perkalianString = tandaAkhir & tempString
    End Function

    Private Function zGetSisa() As Integer
        zGetSisa = varSisa
        varSisa = 0
    End Function

    Private Sub zPutSisa(ByRef berapa As Integer)
        varSisa = berapa
    End Sub

    Private Function zAngkaDibelakangKoma(ByRef sesuatu As String, Optional ByRef karakterKoma As String = ".") As Integer
        If InStr(sesuatu, karakterKoma) = 0 Then
            Return 0
        Else
            Return Len(sesuatu) - InStrRev(sesuatu, karakterKoma)
        End If
    End Function

    Private Function zTanda(ByRef angkaString1 As String, ByVal angkaString2 As String) As String
        Dim retVal As String
        retVal = "-"

        If (InStr(angkaString1, "-") <> 0 And InStr(angkaString2, "-") <> 0) Or
           (InStr(angkaString1, "-") = 0 And InStr(angkaString2, "-") = 0) Then
            retVal = ""
        End If
        zTanda = retVal
    End Function

    Public Function pembagianPembulatanKeatas(ByVal angka As Integer, ByVal pembagi As Integer) As Integer
        Return CInt(IIf(angka Mod pembagi <> 0, angka \ pembagi + 1, angka \ pembagi))
    End Function

    'Public FUNCTION posisiHorTengah(ByRef untukObject, Optional ByRef padaForm, Optional ByRef atauPadaLebar as Integer = 0) as Integer
    '        Dim retVal as Integer

    '        If atauPadaLebar <> 0 Then
    '            retVal = (atauPadaLebar - untukObject.Width) \ 2
    '        Else
    '            retVal = (padaForm.ScaleWidth - untukObject.Width) \ 2
    '        End If
    '        posisiHorTengah = retVal
    '    End Function

    'Public Property Get posisiPalingKanan(ByRef untukObject, ByRef terhadapObject) as Integer
    '    Dim retVal as Integer

    ' If TypeOf terhadapObject Is Screen Then
    '  retVal = terhadapObject.Width - untukObject.Width
    ' Else
    '  retVal = terhadapObject.ScaleWidth - untukObject.Width
    ' End If
    ' posisiPalingKanan = retVal
    'End Property


End Class
